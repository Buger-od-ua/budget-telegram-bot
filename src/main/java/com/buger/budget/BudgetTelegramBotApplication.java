package com.buger.budget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BudgetTelegramBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(BudgetTelegramBotApplication.class, args);
	}

}
